let closeModelEle = document.getElementById("close-menu");
let openModelEle = document.getElementById("burger-icon");
let modalMainContainer = document.getElementById("modal-main-ccontainer");

openModelEle.addEventListener("click", () => {
    modalMainContainer.classList.remove("display-control")
})

closeModelEle.addEventListener("click", () => {
    modalMainContainer.classList.add("display-control");
})

// cart
let cartIcon = document.getElementById("cart-icon");
let cart = document.querySelector(".cart");
let closeCart = document.getElementById("close-cart");
//Open Cart
cartIcon.addEventListener("click", (event) => {
    cart.classList.add("active");

})
//Close cart
closeCart.addEventListener("click", (event) => {
    cart.classList.remove("active");
    
})

// Home page clicks
let homePageEle = document.getElementById("home-page");
let productPageElement = document.getElementById("product-page");
let backEle = document.getElementById("back-btn-1");
let dealsContainer = document.getElementById("dealsSection");
let caresContainer = document.getElementById("cares-section");

//men-section
let menSectionEle = document.getElementById("men-section");
menSectionEle.addEventListener("click", () => {
    homePageEle.classList.add("display-control");
    dealsContainer.classList.add("display-control");
    caresContainer.classList.add("display-control");
    productPageElement.classList.remove("display-control");
})


backEle.addEventListener("click", () => {
    productPageElement.classList.add("display-control");
    homePageEle.classList.remove("display-control");
    dealsContainer.classList.remove("display-control")
    caresContainer.classList.remove("display-control");
})

//women-section
let womenSectionEle = document.getElementById("men-section");
womenSectionEle.addEventListener("click", () => {
    homePageEle.classList.add("display-control");
    productPageElement.classList.remove("display-control");
})


backEle.addEventListener("click", () => {
    productPageElement.classList.add("display-control");
    homePageEle.classList.remove("display-control");
})